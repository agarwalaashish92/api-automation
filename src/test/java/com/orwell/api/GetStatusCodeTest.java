package com.orwell.api;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static com.jayway.restassured.RestAssured.*;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;

public class GetStatusCodeTest {

	@BeforeClass
	public void setBaseUri() {

		RestAssured.baseURI = "http://35.154.179.29";
		RestAssured.port = 9000;
	}

	@Test(priority=1,description = "User Get Request\n"
			+ "1.Post the request \n" 
			+ "2.Validate the response \n")
	public void testStatusCode() {

		Response res = given().when()
				.get("/users").then()
			    .contentType(ContentType.JSON)
			    .extract().response();

		System.out.println(res.asString());
		Assert.assertEquals(res.statusCode(), 200);
	}

}
