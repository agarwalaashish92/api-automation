package com.orwell.api;

import static com.jayway.restassured.RestAssured.given;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;

public class CreateUserwithoutaddress {

	@BeforeClass
	public void setBaseUri() {

		RestAssured.baseURI = "http://35.154.179.29:9000";
		RestAssured.port = 9000;
	}

	@Test(priority = 4,description = "Create User Post Request without Address\n"
			+ "1.Post the request \n" 
			+ "2.Validate the response \n")
	public void postString() throws IOException {

		Response r = given().contentType("application/json")
				.body("{\n" + "	\"username\": \"RahulBansal\",\n" + "	\"password\": \"password\",\n"
						+ "	\"name\": \"User3\",\n" + "	\"email\": \"user3@testingxperts.com\"\n" + "}")
				.when().post("/users");

		String body = r.getBody().asString();
		System.out.println(body);
	}

}
