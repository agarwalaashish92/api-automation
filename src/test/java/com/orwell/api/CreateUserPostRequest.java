package com.orwell.api;

import static com.jayway.restassured.RestAssured.given;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.relevantcodes.extentreports.LogStatus;

import net.minidev.json.JSONObject;

public class CreateUserPostRequest{
	
	@BeforeClass
	public void setBaseUri() {

		RestAssured.baseURI = "http://35.154.179.29:9000";
		RestAssured.port = 9000;
	}

	@Test(priority=2,description = "Create User Post Request\n"
			+ "1.Post the request \n" 
			+ "2.Validate the response \n")
	public void testStatusCode() {

    	Response r = given()
    	.contentType("application/json").
    	body("{\n" + 
    			"	\"username\": \"Aman\",\n" + 
    			"	\"password\": \"password\",\n" + 
    			"	\"name\": \"Aman\",\n" + 
    			"	\"email\": \"manjeet.kumar@testingxperts.com\",\n" + 
    			"	\"mobile\": \"+91-9872709876\",\n" + 
    			"	\"address\": \"Site No. 13-A, Rajiv Gandhi IT Park, Chandigarh\"\n" + 
    			"}").
        when().
        post("/users");

    	String body = r.getBody().asString();
    	System.out.println(body);

	}

}
