package com.orwell.api;

import static com.jayway.restassured.RestAssured.given;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;

public class CreateUserwithaddress {

	@BeforeClass
	public void setBaseUri() {

		RestAssured.baseURI = "http://35.154.179.29:9000";
		RestAssured.port = 9000;
	}

	@Test(priority = 3,description = "Create User Post Request with Address\n"
			+ "1.Post the request \n" 
			+ "2.Validate the response \n")
	public void postString() throws IOException {

		Response r = given().contentType("application/json")
				.body("{\n" + "	\"username\": \"Pankaj\",\n" + "	\"password\": \"password\",\n"
						+ "	\"name\": \"User1\",\n" + "	\"email\": \"user1@testingxperts.com\",\n"
						+ "	\"address\": \"address of User1\"\n" + "}")
				.when().post("/users");

		String body = r.getBody().asString();
		System.out.println(body);
	}

}
