package com.orwell.report;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.testng.IReporter;
import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.xml.XmlSuite;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.ReporterType;

/**
 * Generates the extent report html.
 */
public class ExtentReporter implements IReporter {
	protected static final String EXTENT_CONFIG_FILE = "extent-config.xml";
	protected static final String BASE_PAGES_PACKAGE = "com.bcg.catalyst.pages";

	private ExtentReports extent;

	@Override
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
		String reportFile = System.getProperty("user.dir") + "/target" + File.separator + "extentreport.html";
		extent = new ExtentReports(reportFile, true);
		extent.startReporter(ReporterType.DB, System.getProperty("user.dir") + "/target" + File.separator + "extent.db");
		extent.loadConfig(new File(System.getProperty("user.dir") + "/extent-config.xml"));

		for (ISuite suite : suites) {
			Map<String, ISuiteResult> result = suite.getResults();

			for (ISuiteResult r : result.values()) {
				ITestContext context = r.getTestContext();

				buildTestNodes(context.getPassedTests(), LogStatus.PASS);
				buildTestNodes(context.getFailedTests(), LogStatus.FAIL);
				buildTestNodes(context.getSkippedTests(), LogStatus.SKIP);
			}
		}

		/*
		 * for (String s : Reporter.getOutput()) extent.setTestRunnerOutput(s);
		 */

		Reporter.log("Automation Report: " + reportFile, true);
		extent.flush();
		extent.close();
	}

	private void buildTestNodes(IResultMap tests, LogStatus status) {
		ExtentTest test;

		if (tests.size() > 0) {
			for (ITestResult result : tests.getAllResults()) {
				test = extent.startTest(result.getTestClass().getRealClass().getSimpleName() + " -> "
						+ result.getMethod().getMethodName());
				if (result.getMethod().getDescription() != null)
					test.getTest().setDescription(result.getMethod().getDescription().replace("\n", "<br>"));
				test.getTest().setStartedTime(getTime(result.getStartMillis()));
				test.getTest().setEndedTime(getTime(result.getEndMillis()));

				Object[] parameters = result.getParameters();
				if (parameters != null && parameters.length > 0) {
					String description = test.getTest().getDescription();
					description += "<br>Parameters: ";
					for (Object parameter : parameters)
						description += parameter + " ";
					test.getTest().setDescription(description);
				}

				List<String> testngReporterLogs = Reporter.getOutput(result);
				for (String testngLog : testngReporterLogs)
					test.log(LogStatus.INFO, testngLog);

				// Tag as functional/integration/longerintegration.
				for (String group : result.getMethod().getGroups())
					test.assignAuthor(group);

				// Tag the page being tested using the package name.

				String instanceName = result.getInstanceName();
				instanceName = instanceName.replace(BASE_PAGES_PACKAGE, "");
				String[] pageCategory = instanceName.split("\\.");

				// Assign base package category to test
				if (pageCategory != null && pageCategory.length > 0) {
					test.assignCategory(pageCategory[0]);

					// Assign sub package (1 level down) to test
					if (pageCategory.length > 1)
						test.assignCategory(pageCategory[1]);
				}

				/*for (int index = 0; index < pageCategory.length - 1; index++)
					test.assignCategory(pageCategory[index]);*/
				

				if (result.getThrowable() != null) {
					String throwableMessageSimple = result.getThrowable().getMessage();
					if (throwableMessageSimple != null && !throwableMessageSimple.isEmpty()
							&& throwableMessageSimple.indexOf('\n') > 0) {
						throwableMessageSimple = throwableMessageSimple.substring(0,
								throwableMessageSimple.indexOf('\n'));
						test.log(status, throwableMessageSimple + "<br>Exception: ");
					}
					test.log(status, result.getThrowable());
				} else {
					test.log(status, "Test " + status.toString().toLowerCase() + "ed");
				}

				extent.endTest(test);
			}
		}
	}

	private Date getTime(long millis) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(millis);
		return calendar.getTime();
	}

	
}
